//  Tabs.
let tabTitle = document.querySelectorAll('.services-tabs-item');
let tabContents = document.querySelectorAll('.services-tabs-content-item');
tabTitle.forEach(function (tab) {
	tab.addEventListener('click', function () {
		let id = this.getAttribute('data-tab'),
			content = document.querySelector('.services-tabs-content-item[data-tab="' + id + '"]'),
			activeTitle = document.querySelector('.services-tabs-item.active'),
			activeContent = document.querySelector('.services-tabs-content-item.active');
		activeTitle.classList.remove('active');
		tab.classList.add('active');
		activeContent.classList.remove('active');
		content.classList.add('active');
	});
});





//Section Amazing work
const gallery1 = document.querySelector('.work-gallery-container');
// отримали колекцію
const allImg = Array.from(gallery1.children);

allImg.forEach((item, index) => {
	if (item.getAttribute('data-item') !== 'graphic') {
		item.classList.add('img-hidden');
	}
});